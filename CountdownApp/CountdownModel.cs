﻿using System;

namespace CountdownApp
{
    internal class CountdownModel
    {
        public DateTime EventDate { get; set; }
        public TimeSpan IntervalFromToday { get; set; }
        public string EventDescription { get; set; }
        public int NumberOfWorkingDaysTillEvent { get; set; }
        public int NumberOfWeekendDaysTillEvent { get; set; }
        public int NumberOfWeeksTillEvent { get; set; }

    }
}