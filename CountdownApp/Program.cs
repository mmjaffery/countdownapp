﻿using CountdownApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<CountdownModel> countdownModel = TextParser.ReadDataFile();

            foreach (var item in countdownModel)
            {
                //Console.WriteLine(@"{0}, {1}, Arriving in {2} days", item.EventDate, item.EventDescription, item.EventDate.Subtract(DateTime.Now).TotalDays);
                Console.WriteLine(@"{0}, {1}, Arriving in {2} days", item.EventDate, item.EventDescription, item.IntervalFromToday.Days);
                Console.WriteLine(@"{0}, {1}, Arriving in {2} weekend days", item.EventDate, item.EventDescription, item.NumberOfWeekendDaysTillEvent);
                Console.WriteLine(@"{0}, {1}, Arriving in {2} working days", item.EventDate, item.EventDescription, item.NumberOfWorkingDaysTillEvent);
                Console.WriteLine(@"{0}, {1}, Arriving in {2} working days", item.EventDate, item.EventDescription, item.NumberOfWorkingDaysTillEvent);
                Console.WriteLine(@"{0}, {1}, Arriving in {2} weeks", item.EventDate, item.EventDescription, item.NumberOfWeeksTillEvent);

            }
            Console.ReadLine();

        }


    }
      
}
