﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownApp.Data
{
    public static class TextParser
    {

        internal static List<CountdownModel> ReadDataFile()
        {
            List<CountdownModel> countdownList = new List<CountdownModel>();
            using (TextFieldParser parser = new TextFieldParser(@"C:\Users\Mohammad\source\repos\CountdownApp\CountdownApp\Data\SeedData.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                //iterating rows
                bool firstLine = true;
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();

                    //getcolumn headers
                    if (firstLine)
                    {
                        firstLine = false;
                        continue;
                    }

                    CountdownModel countdownObj = new CountdownModel();
                    countdownObj.EventDate = Convert.ToDateTime(fields[0]);
                    countdownObj.EventDescription = fields[1];
                    countdownObj.IntervalFromToday = DateCalculation.CalculateDays(countdownObj.EventDate);
                    countdownObj.NumberOfWeekendDaysTillEvent = DateCalculation.GetNumberOfWeekendDays(DateTime.Now, countdownObj.EventDate);
                    countdownObj.NumberOfWorkingDaysTillEvent = DateCalculation.GetNumberOfWorkingDays(DateTime.Now, countdownObj.EventDate);
                    countdownObj.NumberOfWeeksTillEvent = DateCalculation.GetNumberOfWeeks(DateTime.Now, countdownObj.EventDate);


                    countdownList.Add(countdownObj);
                }
            }
            return countdownList;
        }

      
    }
}

