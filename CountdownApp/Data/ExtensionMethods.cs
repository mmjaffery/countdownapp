﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownApp.Data
{
    public static class ExtensionMethods
    {
        public static int ToInt(this string strNumber, int defaultInt)
        {
            int resultNum = defaultInt;
            try
            {
                if (!string.IsNullOrEmpty(strNumber))
                    resultNum = Convert.ToInt32(strNumber);
            }
            catch 
            {
            }
            return resultNum;
        }
    }
}
