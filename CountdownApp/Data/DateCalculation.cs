﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountdownApp.Data
{
    public static class DateCalculation
    {
        public static TimeSpan CalculateDays(DateTime eventDate)
        {
            DateTime countdownEventDate = eventDate;
            DateTime dateFromNow = DateTime.Now;

            TimeSpan interval = countdownEventDate - dateFromNow;
            return interval;
        }

        public static int GetNumberOfWorkingDays(DateTime start, DateTime stop)
        {
            int days = 0;

            while (start <= stop)
            {
                if (start.DayOfWeek != DayOfWeek.Saturday && start.DayOfWeek != DayOfWeek.Sunday)
                {
                    ++days;
                }
                start = start.AddDays(1);
            }
            return days;
        }

        public static int GetNumberOfWeekendDays(DateTime start, DateTime stop)
        {
            int days = 0;

            while (start <= stop)
            {
                if (start.DayOfWeek == DayOfWeek.Saturday || start.DayOfWeek == DayOfWeek.Sunday)
                {
                    ++days;
                }
                start = start.AddDays(1);
            }
            return days;
        }

        internal static int GetNumberOfWeeks(DateTime now, DateTime eventDate)
        {
            DateTime datetimeNow = DateTime.Now;
            DateTime myEventDate = eventDate;

            TimeSpan timeSpan = myEventDate - datetimeNow;
            int weeks = (int) ( timeSpan.TotalDays ) / 7;

            return weeks;

        }
    }
}
